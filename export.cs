﻿using System;

public class export
{
	public void export()
	{
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        object misValue = System.Reflection.Missing.Value;

        xlApp = new Excel.Application();
        xlWorkBook = xlApp.Workbooks.Add(misValue);

        for (int i = 0; i < sheetsExport.Count; i++)
        {
            //loop sheet
            List<Tuple<string, List<string>>> currSheet = sheetsExport.Values[i];
            string sheetName = (31 < sheetsExport.Keys[i].Length)
                ? sheetsExport.Keys[i].Substring(0, 31)
                : sheetsExport.Keys[i];

            sheetName = sheetName
                .Replace(':', '_')
                .Replace('/', '_');
                    
            if (i == 0)
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            else
            {
                xlWorkBook.Worksheets.Add();
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            }
                     
            xlWorkSheet.Name = sheetName;

            int colCount = currSheet.Count,
                rowCount = currSheet[0].Item2.Count;

            //insert sheet content
            for (int r = 0; r <= rowCount; r++)
            {
                //loop row
                for (int c = 0; c < colCount; c++)
                {
                    //loop col
                    if (r == 0)
                    {
                        //colHeader
                        xlWorkSheet.Cells[r + 2, c + 1] = currSheet[c].Item1;
                    }
                    else
                    {
                        //colContent
                        xlWorkSheet.Cells[r + 2, c + 1] = currSheet[c].Item2[r - 1];
                    }
                }
            }
            //autofit
            xlWorkSheet.Columns.AutoFit();

            //freeze
            xlWorkSheet.Activate();
            xlWorkSheet.Application.ActiveWindow.SplitRow = 2;
            xlWorkSheet.Application.ActiveWindow.SplitColumn = 3;
            xlWorkSheet.Application.ActiveWindow.FreezePanes = true;

            //color
                    
            Excel.Range firstRow = (Excel.Range)xlWorkSheet.Rows[1];
            firstRow.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White); 

            Excel.Range secondRow = (Excel.Range)xlWorkSheet.Rows[2];
            secondRow.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(229, 175, 66));
            secondRow.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            secondRow.Font.Bold = true;
            secondRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            secondRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;


            Excel.Range threeFirstCols = (Excel.Range)xlWorkSheet.Range["A3", "C" + rowCount + 1];
            threeFirstCols.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(217, 217, 217));

            //lock
            Excel.Range lockRange = (Excel.Range)xlWorkSheet.UsedRange;
            lockRange.Locked = false;
            Excel.Range rowTwo = (Excel.Range)xlWorkSheet.Rows[2];
            rowTwo.Locked = true;
            xlWorkSheet.Range["A3", "C" + rowCount + 1].Locked = true;

            for (int p = 3; p < colCount; p++)
            {
                if (lvParaSelect.FindItemWithText(" " + currSheet[p].Item1).ImageIndex == 1)
                {
                    //readonly
                    Excel.Range currCol = (Excel.Range)xlWorkSheet.Columns[p + 1];
                    currCol.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(247, 91, 97));
                    currCol.Locked = true;
                }
            }

            xlWorkSheet.Protect(UserInterfaceOnly: true);

            //releaseObject(xlWorkSheet);
        }
        xlWorkBook.SaveAs(path, Excel.XlFileFormat.xlOpenXMLWorkbook, misValue, misValue, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, misValue, misValue, misValue);
        xlWorkBook.Close(true, misValue, misValue);
        xlApp.Quit();


        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        MessageBox.Show("Excel file created");
    }

    public void lvCateEle_ItemChecked(object sender, ItemCheckedEventArgs e)
    {
        //get para list from cate or ele
        if (firstClick == false)
        {
            if (cbbTypeSelect.SelectedIndex == 0)
            {
                //category
                if (e.Item.Checked)
                {
                    //checked status
                    string selectItem = e.Item.Text;
                    int index = myCategories.IndexOfKey(selectItem);
                    Category cate = myCategories.Values[index].Item1;

                    BuiltInCategory builtTnCategory = (BuiltInCategory)cate.Id.IntegerValue;
                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    IList<Element> elementsRaw = collector.OfCategory(builtTnCategory).WhereElementIsNotElementType().ToElements();
                    if (elementsRaw.Count > 0)
                    {
                        List<Element> elementsOfCategory = elementsRaw.GroupBy(x => x.Name).Select(x => x.First()).ToList();

                        //get instance para
                        ParameterSet pSetInstance = elementsOfCategory[0].Parameters;
                        //List<AutoDB.Parameter> pSetInstance = elementsOfCategory[0].GetOrderedParameters().ToList();

                        //green -> 0, red -> 1, yellow -> 2
                        //for (int i = 0; i < pSetInstance.Count; i++)
                        foreach (AutoDB.Parameter para in pSetInstance)
                        {
                            //AutoDB.Parameter para = pSetInstance[i];
                            if (lvPara.FindItemWithText(" " + para.Definition.Name) == null &&
                                lvParaSelect.FindItemWithText(" " + para.Definition.Name) == null)
                            {
                                if (para.IsReadOnly)
                                {
                                    ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 1);
                                    tmpItem.ForeColor = unableColor;
                                    tmpItem.Name = "unique";
                                    tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Read-Only" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                    lvPara.Items.Add(tmpItem);
                                }
                                else
                                {
                                    ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 0);
                                    tmpItem.Name = "unique";
                                    tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Instance" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                    lvPara.Items.Add(tmpItem);
                                }
                            }
                            else
                            {
                                ListViewItem tmpItem = lvPara.FindItemWithText(" " + para.Definition.Name);
                                if (tmpItem != null)
                                {
                                    if (tmpItem.Name == "unique")
                                        tmpItem.Name = "";
                                }
                                else
                                {
                                    ListViewItem tmpSelItem = lvParaSelect.FindItemWithText(" " + para.Definition.Name);
                                    if (tmpSelItem != null && tmpSelItem.Name == "unique")
                                        tmpSelItem.Name = "";
                                }
                            }
                        }

                        //get type para
                        Element elementID = doc.GetElement(elementsOfCategory[0].GetTypeId());
                        if (elementID != null)
                        {
                            ParameterSet pSetType = elementID.Parameters;
                            //List<AutoDB.Parameter> pSetType = elementID.GetOrderedParameters().ToList();
                            //for (int i = 0; i < pSetType.Count; i++)
                            foreach (AutoDB.Parameter para in pSetType)
                            {
                                //AutoDB.Parameter para = pSetType[i];
                                if (lvPara.FindItemWithText(" " + para.Definition.Name) == null &&
                                    lvParaSelect.FindItemWithText(" " + para.Definition.Name) == null)
                                {
                                    if (para.IsReadOnly)
                                    {
                                        ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 1);
                                        tmpItem.ForeColor = unableColor;
                                        tmpItem.Name = "unique";
                                        tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Read-Only" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                        lvPara.Items.Add(tmpItem);
                                    }
                                    else
                                    {
                                        ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 2);
                                        tmpItem.Name = "unique";
                                        tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Type" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                        lvPara.Items.Add(tmpItem);
                                    }
                                }
                                else
                                {
                                    ListViewItem tmpItem = lvPara.FindItemWithText(" " + para.Definition.Name);
                                    if (tmpItem != null)
                                    {
                                        if (tmpItem.Name == "unique")
                                            tmpItem.Name = "";
                                    }
                                    else
                                    {
                                        ListViewItem tmpSelItem = lvParaSelect.FindItemWithText(" " + para.Definition.Name);
                                        if (tmpSelItem != null && tmpSelItem.Name == "unique")
                                            tmpSelItem.Name = "";
                                    }
                                }
                            }
                        }


                        //TODO: save a list of chosen categories
                        SheetExportConstruction(elementsRaw.ToList(), selectItem);

                        loadParaAvailable();
                    }
                    else
                    {
                        //TODO: save a list of chosen categories, this category have no elements
                        SheetExportConstruction(new List<Element>(), selectItem);
                    }
                }
                else
                {
                    //unchecked status
                    string selectItem = e.Item.Text;
                    int index = myCategories.IndexOfKey(selectItem);
                    Category cate = myCategories.Values[index].Item1;

                    BuiltInCategory builtTnCategory = (BuiltInCategory)cate.Id.IntegerValue;
                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    IList<Element> elementsRaw = collector.OfCategory(builtTnCategory).WhereElementIsNotElementType().ToElements();
                    if (elementsRaw.Count > 0)
                    {
                        List<Element> elementsOfCategory = elementsRaw.GroupBy(x => x.Name).Select(x => x.First()).ToList();
                        List<ListViewItem> uniqueItems = lvPara.Items.Cast<ListViewItem>().Where(x => x.Name == "unique").ToList();

                        //update instance para
                        ParameterSet pSetInstance = elementsOfCategory[0].Parameters;
                        //List<AutoDB.Parameter> pSetInstance = elementsOfCategory[0].GetOrderedParameters().ToList();
                        //for (int i = 0; i < pSetInstance.Count; i++)
                        foreach (AutoDB.Parameter para in pSetInstance)
                        {
                            //string tmpPName = " " + pSetInstance[i].Definition.Name;
                            string tmpPName = " " + para.Definition.Name;
                            List<ListViewItem> resultCheck = uniqueItems.Where(x => x.Text == tmpPName).ToList();
                            if (resultCheck.Count > 0)
                            {
                                lvPara.Items.Remove(resultCheck[0]);
                            }
                            //get an listviewitem from uniItems -> then remove from lvPara
                        }

                        //update type para
                        Element elementID = doc.GetElement(elementsOfCategory[0].GetTypeId());
                        if (elementID != null)
                        {
                            ParameterSet pSetType = elementID.Parameters;
                            //List<AutoDB.Parameter> pSetType = elementID.GetOrderedParameters().ToList();
                            //for (int i = 0; i < pSetType.Count; i++)
                            foreach (AutoDB.Parameter para in pSetType)
                            {
                                //string tmpPName = " " + pSetType[i].Definition.Name;
                                string tmpPName = " " + para.Definition.Name;
                                List<ListViewItem> resultCheck = uniqueItems.Where(x => x.Text == tmpPName).ToList();
                                if (resultCheck.Count > 0)
                                {
                                    lvPara.Items.Remove(resultCheck[0]);
                                }
                            }
                        }

                    }
                    //check listview have any items or not
                    if (lvCateEle.CheckedItems.Count == 0)
                    {
                        lvPara.Items.Clear();
                        lvParaSelect.Items.Clear();
                    }

                    if (lvPara.Items.Count == 0)
                        paraAvailable.Clear();
                    else
                        loadParaAvailable();
                    //TODO: update a list of chosen categories
                    SheetExportDelSheet(selectItem);
                }
            }
            else
            {
                //elements
                if (e.Item.Checked)
                {
                    //checked status
                    string selectItem = e.Item.Text;
                    Element ele = myElements[selectItem];

                    //get instance para
                    ParameterSet pSetInstance = ele.Parameters;
                    //List<AutoDB.Parameter> pSetInstance = ele.GetOrderedParameters().ToList();
                    //green -> 0, red -> 1, yellow -> 2
                    //for (int i = 0; i < pSetInstance.Count; i++)
                    foreach (AutoDB.Parameter para in pSetInstance)
                    {
                        //AutoDB.Parameter para = pSetInstance[i];
                        if (lvPara.FindItemWithText(" " + para.Definition.Name) == null &&
                            lvParaSelect.FindItemWithText(" " + para.Definition.Name) == null)
                        {
                            if (para.IsReadOnly)
                            {
                                ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 1);
                                tmpItem.ForeColor = unableColor;
                                tmpItem.Name = "unique";
                                tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Read-Only" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                lvPara.Items.Add(tmpItem);
                            }
                            else
                            {
                                ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 0);
                                tmpItem.Name = "unique";
                                tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Instance" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                lvPara.Items.Add(tmpItem);
                            }
                        }
                        else
                        {
                            ListViewItem tmpItem = lvPara.FindItemWithText(" " + para.Definition.Name);
                            if (tmpItem != null)
                            {
                                if (tmpItem.Name == "unique")
                                    tmpItem.Name = "";
                            }
                            else
                            {
                                ListViewItem tmpSelItem = lvParaSelect.FindItemWithText(" " + para.Definition.Name);
                                if (tmpSelItem != null && tmpSelItem.Name == "unique")
                                    tmpSelItem.Name = "";
                            }
                        }
                    }

                    //get type para
                    Element elementID = doc.GetElement(ele.GetTypeId());
                    if (elementID != null)
                    {
                        ParameterSet pSetType = elementID.Parameters;
                        //List<AutoDB.Parameter> pSetType = elementID.GetOrderedParameters().ToList();
                        //for (int i = 0; i < pSetType.Count; i++)
                        foreach (AutoDB.Parameter para in pSetType)
                        {
                            //AutoDB.Parameter para = pSetType[i];
                            if (lvPara.FindItemWithText(" " + para.Definition.Name) == null &&
                                lvParaSelect.FindItemWithText(" " + para.Definition.Name) == null)
                            {
                                if (para.IsReadOnly)
                                {
                                    ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 1);
                                    tmpItem.ForeColor = unableColor;
                                    tmpItem.Name = "unique";
                                    tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Read-Only" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                    lvPara.Items.Add(tmpItem);
                                }
                                else
                                {
                                    ListViewItem tmpItem = new ListViewItem(" " + para.Definition.Name, 2);
                                    tmpItem.Name = "unique";
                                    tmpItem.ToolTipText = "Name: " + para.Definition.Name + Environment.NewLine + "Parameter Type: Type" + Environment.NewLine + "Data Type: " + para.StorageType.ToString() + " (" + para.Definition.ParameterType.ToString() + ")";
                                    lvPara.Items.Add(tmpItem);
                                }
                            }
                            else
                            {
                                ListViewItem tmpItem = lvPara.FindItemWithText(" " + para.Definition.Name);
                                if (tmpItem != null)
                                {
                                    if (tmpItem.Name == "unique")
                                        tmpItem.Name = "";
                                }
                                else
                                {
                                    ListViewItem tmpSelItem = lvParaSelect.FindItemWithText(" " + para.Definition.Name);
                                    if (tmpSelItem != null && tmpSelItem.Name == "unique")
                                        tmpSelItem.Name = "";
                                }
                            }
                        }
                    }

                    //TODO: save a list of chosen elements
                    string cateName = cbbCate.Items[cbbCate.SelectedIndex].ToString();
                    Category cate = myCategories[cateName].Item1;

                    BuiltInCategory builtTnCategory = (BuiltInCategory)cate.Id.IntegerValue;
                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    IList<Element> elementsRaw = collector.WhereElementIsNotElementType().OfCategory(builtTnCategory).ToElements();
                    List<Element> elementsOfCategory = elementsRaw.Where(x => x.Name == ele.Name).ToList();

                    //if (ele.GetTypeId().IntegerValue > 0)
                    //{
                    //    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    //    List<Element> elementList = collector.OfClass(ele.GetType()).ToElements().ToList();
                    //    SheetExportConstruction(elementList, selectItem);
                    //}
                    //else
                    //    SheetExportConstruction(new List<Element>(), selectItem);

                    SheetExportConstruction(elementsOfCategory, selectItem);

                    loadParaAvailable();

                    //lblNumOfEle.Visible = true;
                    //int eleCount = sheetsExport.Values[sheetsExport.Count - 1][0].Item2.Count;
                    //lblEleCount.Text = eleCount.ToString();
                    //if (eleCount > 500)
                    //    lblElementAnnounce.Visible = true;
                }
                else
                {
                    //unchecked status
                    string selectItem = e.Item.Text;
                    Element ele = myElements[selectItem];
                    List<ListViewItem> uniqueItems = lvPara.Items.Cast<ListViewItem>().Where(x => x.Name == "unique").ToList();
                    //update instance para
                    ParameterSet pSetInstance = ele.Parameters;
                    //List<AutoDB.Parameter> pSetInstance = ele.GetOrderedParameters().ToList();
                    //for (int i = 0; i < pSetInstance.Count; i++)
                    foreach (AutoDB.Parameter para in pSetInstance)
                    {
                        //string tmpPName = " " + pSetInstance[i].Definition.Name;
                        string tmpPName = " " + para.Definition.Name;
                        List<ListViewItem> resultCheck = uniqueItems.Where(x => x.Text == tmpPName).ToList();
                        if (resultCheck.Count > 0)
                        {
                            lvPara.Items.Remove(resultCheck[0]);
                        }
                        //get an listviewitem from uniItems -> then remove from lvPara
                    }

                    //update type para
                    Element elementID = doc.GetElement(ele.GetTypeId());
                    if (elementID != null)
                    {
                        ParameterSet pSetType = elementID.Parameters;
                        //List<AutoDB.Parameter> pSetType = elementID.GetOrderedParameters().ToList();
                        //for (int i = 0; i < pSetType.Count; i++)
                        foreach (AutoDB.Parameter para in pSetType)
                        {
                            //string tmpPName = " " + pSetType[i].Definition.Name;
                            string tmpPName = " " + para.Definition.Name;
                            List<ListViewItem> resultCheck = uniqueItems.Where(x => x.Text == tmpPName).ToList();
                            if (resultCheck.Count > 0)
                            {
                                lvPara.Items.Remove(resultCheck[0]);
                            }
                        }
                    }

                    //check listview have any items or not
                    if (lvCateEle.CheckedItems.Count == 0)
                    {
                        lvPara.Items.Clear();
                        lvParaSelect.Items.Clear();
                    }

                    if (lvPara.Items.Count == 0)
                        paraAvailable.Clear();
                    else
                        loadParaAvailable();

                    //TODO: update a list of chosen elements
                    SheetExportDelSheet(selectItem);
                }
            }
        }
    }
}
